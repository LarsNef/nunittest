﻿using System;

namespace ConsoleApp1
{
    public class Calculator
    {
        public string Name { get; set; }
        public Calculator(string name)
        {
            Name = name;
        }

        public double CalculateBMI(double kg, double m) => Math.Round(kg / Math.Pow(m / 100.0, 2), 1);
    }
}
