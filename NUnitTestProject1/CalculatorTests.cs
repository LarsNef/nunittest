﻿using ConsoleApp1;
using NUnit.Framework;

namespace NUnitTestProject1
{
    class CalculatorTests
    {
        public Calculator Calculator { get; set; }
        [SetUp]
        public void Setup()
        {
            Calculator = new Calculator("Casio");
        }

        [Test]
        public void Expected_Name_ToBe_Set()
        {
            Assert.AreEqual(Calculator.Name, "Casio");
        }

        [Test]
        public void Expected_CalculateBMI_ToReturn_Value()
        {
            double BMI = Calculator.CalculateBMI(68.5, 179);
            Assert.AreEqual(BMI, 21.4);
        }

    }
}
