using NUnit.Framework;

namespace NUnitTestProject1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Always_Pass()
        {
            Assert.Pass();
        }

        [Test]
        public void Always_Fail()
        {
            Assert.Fail();
        }
    }
}